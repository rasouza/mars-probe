## Mars Probe
Mars Probe is a CLI application written in Ruby that reads from a file or standard input and executes several Probe instructions in a row.

Even though it's a simple application, it implements Uncle Bob's [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html). Taking SOLID principles into account, it separates the application into layers and uses the Dependency rule to delegate the single responsibility principle to each layer.

### Installation
It requires to have [Ruby](https://www.ruby-lang.org/) installed (any version will do). Otherwise, you can use the included docker-compose to run the application (see [Docker usage](#docker)).
```
bundle install
```

### Usage

#### Installed Ruby
Just run:
```
ruby app.rb < examples/1.txt
```
or
```
ruby app.rb examples/1.txt
```

#### Docker
This repository provides a docker-compose file if you don't have ruby installed. You can use:
```
docker-compose up
```
to run the application and see its output

### Testing
You can see the unit tests by running either:
```
bundle exec rspec
```
or
```
docker-compose run ruby rspec
```

### Important Notes
- Even though not described in the [test](https://docs.google.com/document/d/1G4oMTsvZOP61lXmdQXFIPiy25GjCt8LvVDYJiItCS-U/edit), the application handles `Command not found` and `Out of bound` errors
- No other major assumptions were made throughtout the application
